# Synthesizer

Having a format for quick-and-dirty melody writing is not really useful if there is no way to listen to what you wrote.

## Format description.

```
Element = Octave | NoteGroup
NoteGroup = Note* + Whitespace
Whitespace = ' '|'\n'|'\r'|'\t'
Note = Letter|Dot + Modifier
Modifier = 'b'|'x'
```

An octave is just a number that signifies the power of two it has to be multiplied with. For example, `4C D 3.B` or `4C D .3B` denotes a C and D in the fourth octave, followed by a B in the third octave. 

Every note group consists of multiple notes that all have an identical duration, are played consecutively, and in total consume an entire beat. Note groups are separated by whitespace. To play a triplet, 

A dot signifies that the previous note should be lengthened to include the time slot of that dot. An R indicates a rest. 

## Example tunes

Example 1:

```
R3A4C D.D .DE F.F .FG E.E .DC CD. R3A4C D.D .DE F.F .FG E.E .DC D 
```

Example 2:

```
4C . E G 3B ..4CD C . A . G 5C 4G...FGFG F.EF E . 
```

## Usage:

Install Rust. Then run: 

```
cargo run -- --filename twinkle.txt --octave 0 --bpm 240 | aplay -r 44100
```

Or run 

```
cargo run -- --help
```

for more information.
