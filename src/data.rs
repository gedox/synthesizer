#[derive(Debug)]
pub struct Note {
    pub pitch: i64,
}

#[derive(Debug)]
pub enum NoteOrDot {
    Note(Note),
    Dot,
    Rest,
}

#[derive(Debug)]
pub struct TimedNote {
    pub note: Note,
    pub start: f64,
    pub end: f64,
}

pub fn time(v: Vec<Vec<NoteOrDot>>) -> Vec<TimedNote> {
    let mut res : Vec<TimedNote> = Vec::new();
    let mut rest = false;
    for (m, vec) in v.into_iter().enumerate() {
        let len = vec.len() as f64;
        for (n, note) in vec.into_iter().enumerate() {
            match note {
                NoteOrDot::Dot => {
                    if !rest {
                        let rlen = res.len();
                        res[rlen - 1].end = m as f64 + (n + 1) as f64 / len;
                    }
                }
                NoteOrDot::Note(note) => {
                    rest = false;
                    res.push(TimedNote {

                    note: note,
                    start: m as f64 + n as f64 / len,
                    end: m as f64 + (n + 1) as f64 / len,
                    });
                }
                NoteOrDot::Rest => {rest = true;}
            }
        }
    }
    res
}

impl NoteOrDot {
    pub fn new(pitch: i64) -> Self {
        NoteOrDot::Note (Note {pitch})
    }

    pub fn increment(&mut self, pitch: i64) {
        if let NoteOrDot::Note (Note{pitch : p}) = self {
            *p += pitch;
        }
    }
}

pub struct PitchConfig {
    pub base: f64,
    pub div: f64,
    pub octave: u64,
    pub resolution: u64,
}

impl Default for PitchConfig {
    fn default() -> Self {
        PitchConfig {
            base: 440.0,
            div: 2.0,
            octave: 12,
            resolution: 44100,
        }
    }
}

pub fn samples(note: &Note, config: &PitchConfig) -> f64 {
    let power = note.pitch as f64 / config.octave as f64;
    config.resolution as f64 / config.base / config.div.powf(power)
}
