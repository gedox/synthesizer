use data::*;

pub enum Element {
    Letter(u8),
    Modifier(u8),
    Number(i64),
    Space,
    Dot
}

pub struct Lexer<'a> {
    data: &'a [u8],
}

impl<'a> Lexer<'a> {
    pub fn new(data: &'a [u8]) -> Self {
        Lexer { data }
    }

    fn poll(&mut self) -> Option<u8> {
        self.data.get(0).cloned()
    }

    fn consume(&mut self) -> Option<u8> {
        if let Some(res) = self.poll() {
            self.data = &self.data[1..];
            Some(res)
        } else {
            None
        }
    }

    pub fn read(&mut self) -> Option<Element> {
        if let Some(u) = self.poll() {
            match u {
                b'0'..=b'9'|b'-' => {
                    let mut res: i64 = 0;
                    while let Some(t) = self.poll() {
                        match t {
                            b'0'..=b'9' => {
                                res *= 10;
                                res += (t - b'0') as i64;
                                self.consume();
                            }
                            b'-' => {res = -res; self.consume();}
                            _ => break,
                        }
                    }
                    Some(Element::Number(res))
                }
                b'A'..=b'G' => {
                    self.consume();
                    Some(Element::Letter(u))
                }
                b' ' | b'\n' | b'\r' | b'\t' => {
                    self.consume();
                    Some(Element::Space)
                }
                b'.' => {
                    self.consume();
                    Some(Element::Dot)
                }
                b'R' => {
                    self.consume();
                    Some(Element::Letter(b'R'))
                }
                e => {
                    self.consume();
                    Some(Element::Modifier(e))
                }
            }
        } else {
            None
        }
    }
}

pub struct Parser<'a> {
    octave: i64,
    lexer: Lexer<'a>,
    result: Vec<Vec<NoteOrDot>>,
}

impl<'a> Parser<'a> {
    pub fn new(lexer: Lexer<'a>) -> Self {
        Parser {
            octave: 0,
            lexer,
            result: vec![Vec::new()],
        }
    }

    fn last_vec(&mut self) -> Option<&mut Vec<NoteOrDot>> {
        let len = self.result.len();
        self.result.get_mut(len - 1)
    }

    fn last(&mut self) -> Option<&mut NoteOrDot> {
        if let Some(t) = self.last_vec() {
            let len = t.len();
            if len == 0 {
                None
            } else {
                t.get_mut(len - 1)
            }
        } else {
            None
        }
    }

    fn get_note(&self, letter: u8) -> NoteOrDot {
        let list = &[12i64, 14, 3, 5, 7, 8, 10] as &[_];
        let index = (letter - b'A') as usize;
        NoteOrDot::new(self.octave * 12 + list[index])
    }

    pub fn parse(mut self) -> Vec<Vec<NoteOrDot>> {
        while let Some(element) = self.lexer.read() {
            match element {
                Element::Letter(b'R') => {
                    self.last_vec().map(|v| v.push(NoteOrDot::Rest));
                }
                Element::Number(n) => self.octave = n,
                Element::Letter(l) => {
                    let note = self.get_note(l);
                    if let Some(vec) = self.last_vec() {
                        vec.push(note);
                    }
                }
                Element::Modifier(m) => if let Some(n) = self.last() {
                    match m {
                        b'x' => n.increment(1),
                        b'b' => n.increment(-1),
                        _ => (),
                    }
                },
                Element::Space => {
                    self.result.push(Vec::new());
                }
                Element::Dot => {
                    self.last_vec().map(|v| v.push(NoteOrDot::Dot));
                }
            }
        }
        self.result.retain(|v| !v.is_empty());
        self.result
    }
}
