use data::Note;
use data::PitchConfig;
use data::samples;

pub trait Generator {
    fn play(&self, note: &Note, config: &PitchConfig, t: i64) -> f64;
}

pub struct SineWaveGenerator;

impl Generator for SineWaveGenerator {
    fn play(&self, note: &Note, config: &PitchConfig, t: i64) -> f64 {
        let length = samples(note, config) as f64;
        (t as f64 * 6.283185 / length).sin()
    }
}

pub struct SawToothGenerator;

impl Generator for SawToothGenerator {
    fn play(&self, note: &Note, config: &PitchConfig, t: i64) -> f64 {
        let length = samples(note, config) as f64;
        let d = t as f64 / length;
        (d - d.floor() - 0.5).abs()*2.0 - 0.5
    }
}

pub struct SemiGenerator<G : Generator>(pub G);

impl<G : Generator> Generator for SemiGenerator<G> {
    fn play(&self, note: &Note, config: &PitchConfig, t: i64) -> f64 {
        (self.0.play(note, config, t) * 3.14/2.0).sin()
    }
}

pub trait Window {
    fn modifier(&self, size: u64, t: i64) -> f64;
    fn buffer(&self) -> f64;
}

pub struct DefaultWindow {
    ramp_units: u64,
}

impl DefaultWindow {
    pub fn new(ramp_units: u64) -> DefaultWindow {
        DefaultWindow { ramp_units }
    }
}

impl Window for DefaultWindow {
    fn buffer(&self) -> f64 {
        self.ramp_units as f64
    }
    fn modifier(&self, size: u64, t: i64) -> f64 {
        let rel_t = t as f64 / size as f64;
        let rel_ramp = self.ramp_units as f64 / size as f64;
        if rel_t < 0.5 {
            if rel_t < -rel_ramp {
                0.0
            } else if rel_t < rel_ramp {
                0.5 + 0.5 * rel_t / rel_ramp
            } else {
                1.0
            }
        } else {
            if rel_t < 1.0 - rel_ramp {
                1.0
            } else if rel_t < 1.0 + rel_ramp {
                0.5 + 0.5 * (1.0 - rel_t) / rel_ramp
            } else {
                0.0
            }
        }
    }
}


pub struct GeneralWindow {
    ramp_up : u64,
    ramp_down : u64,
    relative_start : f64,
    decay : u64,
    fade_out : u64,
}

impl GeneralWindow {
    pub fn new(pos : [u64; 3], level : f64, decay : u64) -> Self {
        GeneralWindow {
            ramp_up : pos[0],
            ramp_down : pos[1],
            relative_start : level,
            decay : decay,
            fade_out : pos[2],
        }
    }
}

impl Window for GeneralWindow {
    fn buffer(&self) -> f64 {
        self.ramp_up as f64
    }
    fn modifier(&self, size: u64, t: i64) -> f64 {
        let rel_t = t as f64 / size as f64;
        if rel_t > 1.0 {return 0.0;}
        let rel_ru = self.ramp_up as f64 / size as f64;
        let rel_rd = self.ramp_down as f64 / size as f64;
        let rel_fo = self.fade_out as f64 / size as f64;
        let ru_value = 1.0 + rel_t / rel_ru;
        let l_value = (rel_rd - rel_t) / self.decay as f64 * size as f64 + self.relative_start;
        let rd_value = 1.0 - rel_t * (1.0 - self.relative_start) / rel_rd;
        let fo_value = (1.0 - rel_t) / rel_fo * self.relative_start;
        let valley = if l_value > rd_value {l_value} else {rd_value};
        let q = if valley > fo_value {fo_value} else {valley};
        let r = if q > ru_value {ru_value} else {q};
        if r < 0.0 {0.0} else {r}
    }
}
