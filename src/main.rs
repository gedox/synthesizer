
#[macro_use] extern crate structopt;

use structopt::StructOpt;

mod data;
mod generator;
mod parser;
mod player;

use std::io;
use std::io::prelude::*;
use std::fs;
use std::error::Error;

#[derive(StructOpt)]
#[structopt(about="Does stuff")]
struct Args {
    #[structopt(long = "filename", short ="f", help="Input file (Default stdin)")]
    filename : Option<String>,
    #[structopt(long = "octave", short="o", help="Base octave (Default 0)")]
    octave : Option<i32>,
    #[structopt(long = "bpm", short = "b", help="Beats per minute (Default 120)")]
    bpm : Option<u64>,
}

fn main() -> Result<(), Box<Error>>  {
    let args = Args::from_args();
    let mut data = Vec::new();
    if let Some(filename) = args.filename {
        let mut file = fs::File::open(filename)?;
        file.read_to_end(&mut data)?;
    } else {
        let mut input = io::stdin();
        input.read_to_end(&mut data)?;
    }
    let octave = args.octave.unwrap_or(0);
    let bpm = args.bpm.unwrap_or(120);
    let lexer = parser::Lexer::new(&data);
    let parser = parser::Parser::new(lexer);
    let notes = parser.parse();
    let size = notes.len();
    let timednotes = data::time(notes);
    let generator = generator::SemiGenerator(generator::SawToothGenerator);
    let units_per_note = 44100*60/bpm;
    // let window = generator::DefaultWindow::new(units_per_note / 10);
    let window = generator::GeneralWindow::new([units_per_note / 20, units_per_note/10, units_per_note / 100], 0.4, units_per_note * 4);
    let config = data::PitchConfig {
        base : 440.0/16.0 * (2.0f64).powi(octave),
        ..Default::default()
    };
    let player = player::Player::new(generator, window, units_per_note , config);
    let res = player.play(timednotes, size as u64);
    let out = io::stdout();
    out.lock().write_all(&res)?;
    Ok(())
}
