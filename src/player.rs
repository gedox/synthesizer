use data::PitchConfig;
use data::TimedNote;

use generator::Generator;
use generator::Window;

pub struct Player<G: Generator, W: Window> {
    generator: G,
    window: W,
    unit: u64,
    config: PitchConfig,
}

impl<G: Generator, W: Window> Player<G, W> {
    pub fn new(generator: G, window: W, unit: u64, config: PitchConfig) -> Self {
        Player {
            generator,
            window,
            unit,
            config,
        }
    }

    pub fn play(&self, vec: Vec<TimedNote>, size: u64) -> Vec<u8> {
        if vec.is_empty() {
            return Vec::new();
        }
        let buffer = self.window.buffer() as i64;
        let mut res = vec![0.0; (size * self.unit + 2*buffer as u64 + 1) as usize];
        for note in vec {
            let start = (note.start * self.unit as f64) as i64;
            let end = (note.end * self.unit as f64) as i64;
            for i in start - buffer..=end + buffer {
                let height = self.generator.play(&note.note, &self.config, i - start);
                let height = height * self.window.modifier((end - start) as u64, i - start);
                res[(i + buffer) as usize] += height;
            }
        }
        let max = res.iter()
            .map(|t| t.abs())
            .fold(0.0, |a, b| if a > b { a } else { b });
        res.into_iter()
            .map(|f| f * 127.5 / max + 127.5)
            .map(|f| f.round() as u8)
            .collect()
    }
}
